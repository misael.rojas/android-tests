package pmx.forms.vialux.pmxforms;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class superListView extends AppCompatActivity {

    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_list_view);

        Bands bands_data[] = new Bands[]{
          new Bands(R.drawable.meme2, "Ikimono Gakari"),
          new Bands(R.drawable.meme2, "High and mighty Color"),
          new Bands(R.drawable.meme2, "Maximun the Hormone"),
          new Bands(R.drawable.meme2, "Morning Musume"),
          new Bands(R.drawable.meme2, "AKB48"),
          new Bands(R.drawable.meme2, "Scandal"),
          new Bands(R.drawable.meme2, "Baby Metal"),
          new Bands(R.drawable.meme2, "Buono"),
          new Bands(R.drawable.meme2, "Geisha Band"),
          new Bands(R.drawable.meme2, "kotak"),
        };

        BandsAdapter adapter = new BandsAdapter(this, R.layout.list_view_row, bands_data);
        lv = (ListView) findViewById(R.id.bandsList);
        View header = (View) getLayoutInflater().inflate(R.layout.list_view_header, null);
        lv.addHeaderView(header);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedFromList = lv.getItemAtPosition(position).toString();
                Toast.makeText(getApplicationContext(), selectedFromList + ", Position " + position, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
