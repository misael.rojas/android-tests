package pmx.forms.vialux.pmxforms;

import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class secondActivity extends AppCompatActivity implements View.OnClickListener, SensorEventListener  {
    TextView chumauch, pSensorTV;
    Button BtnBack;

    LinearLayout ln;
    SensorManager sm;
    Sensor sr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        chumauch = (TextView) findViewById(R.id.chumauch);

        ln = (LinearLayout) findViewById(R.id.linear);
        pSensorTV = (TextView) findViewById(R.id.pSensor);

        sm = (SensorManager)getSystemService(SENSOR_SERVICE);
        sr = sm.getDefaultSensor(Sensor.TYPE_PROXIMITY);

        if(extras!=null){
            String churros = extras.getString("churro");
            chumauch.setText(churros);
        }

        BtnBack = (Button) findViewById(R.id.BtnBack);
        BtnBack.setOnClickListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own actionn", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    protected void onResume() {
        super.onResume();
        sm.registerListener(this, sr,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.BtnBack:
                Intent intent = new Intent(secondActivity.this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        String teks = String.valueOf(event.values[0]);
        pSensorTV.setText(teks);

        float nilai = Float.parseFloat(teks);

        if (nilai == 0) {
            ln.setBackgroundColor(Color.BLUE);
        } else {
            ln.setBackgroundColor(Color.RED);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
