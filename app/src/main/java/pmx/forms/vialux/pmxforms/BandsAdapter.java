package pmx.forms.vialux.pmxforms;

import android.app.Activity;
import android.content.Context;
import android.support.v4.media.session.IMediaControllerCallback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by niigaki on 29/08/16.
 */
public class BandsAdapter extends ArrayAdapter<Bands> {
    Context context;
    int layoutResourceId;
    Bands data[] = null;

    public BandsAdapter(Context context, int layoutResourceId , Bands[] data) {
        super(context, layoutResourceId, data);

        this.context=context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    public View getView(int position, View converView, ViewGroup parent) {
        View row = converView;
        BandsHolder holder = null;

        if(row==null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new BandsHolder();
            holder.imagen = (ImageView) row.findViewById(R.id.imageList);
            holder.text = (TextView) row.findViewById(R.id.tvList);
            row.setTag(holder);
        }else{
            holder=(BandsHolder)row.getTag();
        }

        Bands bands = data[position];
        holder.text.setText(bands.title);
        holder.imagen.setImageResource(bands.icon);

        return row;
    }

    static class BandsHolder{
        ImageView imagen;
        TextView text;
    }
}
