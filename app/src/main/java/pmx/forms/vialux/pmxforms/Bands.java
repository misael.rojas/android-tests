package pmx.forms.vialux.pmxforms;

/**
 * Created by niigaki on 29/08/16.
 */
public class Bands {

    public int icon;
    public String title;

    public Bands(){
        super();
    }

    public Bands(int icon, String title){
        super();
        this.icon = icon;
        this.title = title;
    }

}
