package pmx.forms.vialux.pmxforms;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class churrosActivity extends AppCompatActivity implements View.OnClickListener{
    TextView andaLaOsaTextView;
    EditText andaLaOsaEditText;
    Button andaLaOsaButton, andaLaOsaButton2, goToActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_churros);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        andaLaOsaTextView = (TextView) findViewById(R.id.superText);
        andaLaOsaEditText = (EditText) findViewById(R.id.superEdit);
        andaLaOsaButton = (Button) findViewById(R.id.superButton);
        andaLaOsaButton2 = (Button) findViewById(R.id.superButton2);
        goToActivity = (Button) findViewById(R.id.goToActivity);

        andaLaOsaButton.setOnClickListener(this);
        andaLaOsaButton2.setOnClickListener(this);
        goToActivity.setOnClickListener(this);

        andaLaOsaTextView.setText("que rollo con el pollo");
        andaLaOsaTextView.setText("Que onda");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.superButton:
                String dato = andaLaOsaTextView.getText().toString();
                andaLaOsaEditText.setText(dato);
                Toast.makeText(getApplicationContext(),"you have clicked the first button", Toast.LENGTH_SHORT).show();

                break;
            case R.id.superButton2:
                andaLaOsaEditText.setText("Anda la osa");
                Toast.makeText(getApplicationContext(),"you have clicked the second button", Toast.LENGTH_LONG).show();
                break;

            case R.id.goToActivity:
                Intent intent = new Intent(churrosActivity.this, secondActivity.class);
                String churro = andaLaOsaEditText.getText().toString();
                intent.putExtra("churro", churro);
                startActivity(intent);
                break;
        }

    }

}
